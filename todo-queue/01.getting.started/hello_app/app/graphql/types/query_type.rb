module Types
  class QueryType < Types::BaseObject

    field :items,
      [Types::ItemType],
      null: false,
      description: 'Return a list of items'
  end

  def items
    Item.all
  end
end
