module Types
  class HighScoreType < Types::BaseObject
    field :id, ID, null: false
    field :game, String, null: true
    field :score, Integer, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :game, Types::StringType, null: true
    field :score, Types::IntegerType, null: true
  end
end
