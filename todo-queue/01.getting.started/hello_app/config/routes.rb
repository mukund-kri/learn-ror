Rails.application.routes.draw do


  mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"

  post "/graphql", to: "graphql#execute"

  resources :high_scores
  get 'articles/index'
  root 'application#hello'
end
