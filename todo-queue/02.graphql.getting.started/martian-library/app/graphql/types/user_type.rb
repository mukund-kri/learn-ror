module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :email, String, null: true
    field :full_name, String, null: false

    def full_name 
      [object.last_name, object.first_name].compact.join(', ')
    end
  end
end
