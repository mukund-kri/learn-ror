

#### Install the driver

Put the following dependency in your Gemfile. This should install the postgres driver when your run `db:migrate` in a couple of sections.

```ruby
gem 'pg', '~> 1.2.3'
```

#### Create `role` in postgres with create permissions

```sql
CREATE ROLE blogapp WITH CREATEDB LOGIN PASSWORD 'password';
```

#### point `database.yml` to postgres

change database.yml to look like this ...

```yml
default: &default
  adapter: postgresql
  encoding: unicode
  pool: 5
  username: blogapp
  password: password

development:
  <<: *default
  database: blogapp_dev

test:
  <<: *default
  database: blogapp_test

production:
  <<: *default
  database: blogapp_prod
```

#### creating databases

```shell
rake db:setup
```

Should create the development, test and production databases.


#### run initial migration

Run `db:migrate` to setup migration metadata on in the database.

```shell
rake db:migrate
```

## Reference

[Tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-ruby-on-rails-with-postgres)