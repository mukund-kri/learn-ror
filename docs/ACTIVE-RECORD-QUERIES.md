

#### `find_by` function 

`find_by` queries the db with the parameters and returns only the first record. 

```Ruby
Cat.find_by(color: "grey")
```

Which translates to the following SQL.

```SQL
SELECT "cats".* FROM "cats" WHERE "cats"."color" = 'grey' LIMIT 1
```

Note: the limit is 1. All `find_by` queries the limit is set to 1.


#### `where` function

The where function roughly correspond to SQL `where` clause. And unlike `find_by` it return **all** the matching records.

Example: find all the cats that are grey. 

```Ruby
Cat.where(color: 'grey')
```

Which translates to the following SQL.

```SQL
SELECT "cats".* FROM "cats" WHERE "cats"."color" = 'grey'
```

**Note:** `LIMIT` is absent in the where function.


#### `all`, `first` and `last` functions

* The `all` function returns all the records in a table.
* The `first` function returns the first record in a table.
* The `last` function returns the last record in a table.

Examples:

```Ruby
Cat.all     #=> Returns a list of all cats in the table
Cat.first   #=> Returns cat object, this is the first row in the table
Cat.last    #=> Returns cat object, this is the last row in the table
```

#### `limit` and `offset`


#### `ordering`

### Less commonly  used methods

#### `delete_all` method

#### `distinct` or `uniq`


### Aggregation

#### `count`

#### `group`

## References

[The Odin Project](https://www.theodinproject.com/courses/ruby-on-rails/lessons/active-record-queries)

[RIP Tutorial](https://riptutorial.com/ruby-on-rails/topic/2154/activerecord-query-interface)

[Rails Official Documentation](https://api.rubyonrails.org/classes/ActiveRecord/QueryMethods.html)

[Interesting YouTube video](https://www.youtube.com/watch?v=1ZkwvPMiq4Q)