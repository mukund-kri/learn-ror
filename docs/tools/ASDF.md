# ASDF

A tool to manage multiple runtimes.

## Installation

Installing asdf is very easy. Follow the instruction given [here](https://asdf-vm.com/#/core-manage-asdf?id=install).

## Ruby

In this section let's see how we can install and manage the ruby runtime with asdf. At the time of writing this article ruby 3.0 has just been released. Let's install a ruby 3.0 environment.

#### Install the plugin
For any runtime including ruby we first need to install the ruby plugin. We can do it with

```shell
$ asdf plugin add ruby
```

#### Install the ruby environment

First lets check out what versions of ruby available for install. The following command list all the ruby runtime candidates that are available for install.

```shell
$ asdf list all ruby
```

Wow, the out shows there are hundreds of candidates available for install. We'll just choose ruby 3.0.0 for this exercise.

Next we install the ruby 3.0.0 environment with ...

```shell
$ asdf install ruby 3.0.0
```

This command is going to take some time, as it downloads, compiles and installs a new ruby interpreter and tools.

#### Using the environment

Just like rvm, rbenv, pyenv etc. we can attach an environment/runtime to a directory. So that every time you `cd` into that directory the ruby environment is automatically switched.

Let's set the ruby environment for directory `test` to version `3.0.0`.

```shell
$ cd test
$ asdf local ruby 3.0.0
```

So every time you cd into the `test` directory, the ruby version will default to `3.0.0`. You can check it with ...

```shell
$ ruby --version
```

You should see output like this ...
```
ruby 3.0.0p0 (2020-12-25 revision 95aff21468) [x86_64-linux]
```

## Node

The magic of asdf is that the very same commands we used for ruby will work for node too. So first up install the node plugin.

#### Plugin

```shell
$ asdf plugin add nodejs
```

#### Installing the node environment

First list the node versions available 

```shell
$ asdf list all nodejs
```

Then install the version you need

```shell
$ asdf install nodejs 14.15.4
```
